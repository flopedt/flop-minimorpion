/***************/
/* static grid */
/***************/
/*
   ______ (0,0)
  |
  v                         x
  o-----+-----+-----+------->
  |     |     |     |
  |     |     |     |
  +-----+-----+-----+
  |     |     |     |
  |     |     |     |
  +-----+-----+-----+
  |     |     |     |
  |     |     |     |
  +-----+-----+-----+
  |     <----->
  |    len_square
  |
  v
y

*/


// ---------------------------
// Grid Layout
// ---------------------------
var len_square = 100 ;

d3.select("svg#grille")
    .append("line")
    .attr("stroke", "black")
    .attr("stroke-width", 2)
    .attr("x1", len_square)
    .attr("y1", 0)
    .attr("x2", len_square)
    .attr("y2", 3*len_square);
d3.select("svg#grille")
    .append("line")
    .attr("stroke", "black")
    .attr("stroke-width", 2)
    .attr("x1", 2*len_square)
    .attr("y1", 0)
    .attr("x2", 2*len_square)
    .attr("y2", 3*len_square);
d3.select("svg#grille")
    .append("line")
    .attr("stroke", "black")
    .attr("stroke-width", 2)
    .attr("x1", 0)
    .attr("y1", len_square)
    .attr("x2", 3*len_square)
    .attr("y2", len_square);
d3.select("svg#grille")
    .append("line")
    .attr("stroke", "black")
    .attr("stroke-width", 2)
    .attr("x1", 0)
    .attr("y1", 2*len_square)
    .attr("x2", 3*len_square)
    .attr("y2", 2*len_square);

// ---------------------------
// Draw the pawns
// ---------------------------

var radius = 40 ;
var defaultColor = "#fafafa";
var game = new Object();
game.players = [{"name":"FP","color":"#00FFFF"},
                {"name":"RB","color":"#FFFF00"}
               ];
game.cells = [{"x":0,"y":0, "p":"FP"},
              {"x":0,"y":1, "p":""},
              {"x":0,"y":2, "p":""},
              {"x":1,"y":0, "p":"FP"},
              {"x":1,"y":1, "p":"RB"},
              {"x":1,"y":2, "p":""},
              {"x":2,"y":0, "p":"RB"},
              {"x":2,"y":1, "p":""},
              {"x":2,"y":2, "p":"RB"}
             ];

// create a Map to represent players : [name]=>{name,color}
var players = new Map();
for (var i=0; i<game.players.length; i++ ) {
  players.set( game.players[i].name , game.players[i] );
}
// returns the circle center x coordinate
function move_x(m) {
    return m.x * len_square + len_square/2 ;
}
// returns the circle center y coordinate
function move_y(m) {
    return m.y * len_square + len_square/2 ;
}
// returns the color of the player that played this move
function move_color(m) {
  var playerName = m.p;
  if ( players.has(playerName) ) {
    return players.get(playerName).color;
  } else {
    return defaultColor;
  }
}

console.log("On est prêt pour une partie !");

