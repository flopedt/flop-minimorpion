import json

from django.shortcuts import render

from django.http import HttpResponse, JsonResponse

from .utils import Game

# Create your views here.
def hello(request):
    return HttpResponse("Salut !")


def play(request):
    player1 = request.GET.get('p1','FP')
    player2 = request.GET.get('p2','RB')
    
    return render(request, 'main_app/morpion.html',
                  {'player1': player1,
                   'player2': player2})


def save_game(request):
    game = Game(json.loads(request.POST.get('game',{})))
    game.print_game()
    game.save()
    
    return HttpResponse(status=204)    

def load_game(request):
    player1 = request.GET.get('p1','FP')
    player2 = request.GET.get('p2','RB')

    game = Game()
    
    try:
        game.load(player1, player2)
        game.print_game()
    except ObjectDoesNotExist as e:
        print(e)

    return JsonResponse(game.export_to_json(None),
                        safe=False)
