import numpy as np
import json

from main_app.models import Player, LastGame, Ranking

class Game:
    """ A class to manipulate morpion games 

    Attributes
    ----------
    grid: numpy.array
        each cell contains the index of the player, -1 otherwise
    players: list
        a string list that contains the name of the players

    Methods
    -------
    print_grid(self): print only the grid
    print_game(self): print the grid, the players, and the state of the game
    export_to_json(self, colors = None): export to readable format for the js
    default_game_json(): create dummy game
    is_balanced(self): number of moves ok?
    is_full(self): all cells filled?
    get_winner(self): compute the winner if any
    save(self): save to the database
    load(self, player1, player2): load from the database
    """
    
    def __init__(self, game_json = None):
        """ Constructor that creates a default game or translates
        a json file into a Game

        Parameters
        ----------
        game_json: str, optional
            if provided,
            format : {"players": list of extended_players,
                      "cells": list of cells}
                where 
                - extended_player format:
                      {"name": str name of the player,
                       "color": str color of player's pawn}
                - cell format:
                      {"x": int x coordinate,
                       "y": int y coordinate,
                       "p": int index of the pawn's player}
        """
        self.grid = np.full((3,3),-1)
        if game_json is None:
            self.players = ['FP', 'RB']
        else:
            if len(game_json['players']) != 2:
                raise Exception('How many players in this game?')
            self.players = game_json['players']

            cells = game_json['cells']
            for c in cells:
                if c['y'] < 0 or c['y'] > 2 or c['x'] < 0 or c['x'] > 2:
                    raise Exception(f'Wrong cell number {c}')
                if c['p'] != '':
                    if c['p'] not in self.players:
                        raise Exception(f'Unexpected player: {c["p"]}')
                    iplayer = self.players.index(c['p'])
                    self.grid[c['y'], c['x']] = iplayer


    def print_grid(self):
        """ Prints the grid 
        """
        for line in range(0, 3):
            end = '|'
            for column in range(0, 3):
                iplayer = self.grid[line, column]
                player = self.players[iplayer] if iplayer>=0 else ''
                if column == 2:
                    end='\n'
                print(f'{player:^4}', end=end)
            if line<2:
                print(((4+1)*3-1)*'-')


    def print_game(self):
        """ Prints the grid and the result
        """
        self.print_grid()

        if not self.is_balanced():
            print('Invalid game: the number of moves is imbalanced!!')
            return

        code_winner = self.get_winner()
        if code_winner == -1:
            print(f'Invalid game: someone won twice or two players won')
        elif code_winner == 2:
            print('This is a tie.')
            if not self.is_full():
                print('But the game is not finished yet!')
        else:
            print(f'Congratulations to {self.players[code_winner]}')


    def export_to_json(self, colors):
        """ Exports the current Game to a json-formatted object 
        
        Parameters
        ----------
        colors: list
            a list of 2 colors in hexa

        Returns
        -------
        json object
        """
        if colors is None or len(colors) != 2:
            default_colors = ["#00FFFF", "#FFFF00"]
            try:
                players_db = []
                players_db.append(Player.objects.get(username=self.players[0]))
                players_db.append(Player.objects.get(username=self.players[1]))
                colors = [p.color for p in players_db]
            except ObjectDoesNotExist:
                colors = default_colors
        
        game = {}
        extended_players = []

        # Players part
        for p in range(2):
            extended_players.append({"name": self.players[p],
                                     "color": colors[p] })
        game['players'] = extended_players

        # Grid part
        cells = []
        for line in range(0,3):
            for column in range(0,3):
                iplayer = self.grid[line, column]
                cells.append({"x":column,
                              "y":line,
                              "p":self.players[iplayer] if iplayer > -1 else ""})
        game['cells'] = cells
        
        return game

    
    def default_game_json():
        """
        Returns
        -------
        json-formatted object with a default empty grid and default players

        """
        return Game().export_to_json(["#00FFFF", "#FFFF00"])

    default_game_json = staticmethod(default_game_json)


    def is_balanced(self):
        """ Is the number of moves balanced (each player played almost
        the same number of moves)? 

        Returns
        -------
        boolean
        """
        moves = [0, 0]

        for iplayer in np.nditer(self.grid):
            if iplayer > -1:
                moves[iplayer] += 1

        return abs(moves[0] - moves[1]) <= 1


    def is_full(self):
        """ Is the grid full? 

        Returns
        -------
        boolean
        """
        return -1 not in np.nditer(self.grid)
    

    def get_winner(self):
        """ Finds whether someone won, and who
        Returns
        -------
        -1 if the game is invalid
        0<=i<=1 the index of the winning player
        2  if the game is a tie
        """
        combinations = {}
        # check the lines
        for line in range(3):
            key = f'l{line}'
            combinations[key] = set(self.grid[line, :])
        # check the columns
        for column in range(3):
            key = f'c{column}'
            combinations[key] = set(self.grid[:, column])
        # check the diagonals
        key = 'd1'
        combinations[key] = set()
        for d in range(3):
            combinations[key].add(self.grid[d, d])
        key = 'd2'
        combinations[key] = set()
        for d in range(3):
            combinations[key].add(self.grid[d, 2-d])

        winners = []
        for s in combinations.values():
            if len(s) == 1 and -1 not in s:
                winners.append(s.pop())
            if len(winners) > 1:
                return -1

        if len(winners) == 0:
            return 2

        return winners.pop()
        

    def save(self):
        """ Checks whether the game is finished

        Raises
        ------
        ObjectDoesNotExist if a player is not in the database
        ImbalancedGameException if the game is imbalanced
        SeveralWinnersException if there are several 3 pawns alignments

        """
        players_db = []

        # fill players_db with the two Player objects

        #...
        
        if not self.is_balanced():
            raise ImbalancedGameException()

        code_winner = self.get_winner()
        if code_winner == -1:
            raise SeveralWinnersException()
            
        # the last game needs to be forgotten
        # delete it 

        #...

        if code_winner == 2:
            if not self.is_full():
                # the game is not finished yet:
                # we store the state in the DB
                for line in range(3):
                    for column in range(3):
                        # create a LastGame object to store a cell of the game

                        # game_cell = ...

                        iplayer = self.grid[line, column]
                        if iplayer > -1:
                            game_cell.cell_player = players_db[iplayer]
                            game_cell.save()
            else:
                # the game is finished:
                # we store the result in the DB
                for p in players_db:
                    rank, created = Ranking.objects.get_or_create(player=p)
                    rank.ties += 1
                    rank.save()
        else:
            rank_winner, c = Ranking.objects.get_or_create(player=players_db[code_winner])
            # update rank_winner
            # ...
            # and save it
            # ...
            code_loser = (code_winner + 1) % 2
            rank_loser, c = Ranking.objects.get_or_create(player=players_db[code_loser])
            rank_loser.defeats += 1
            rank_loser.save()

        for p in players_db:
            rank, c = Ranking.objects.get_or_create(player=p)
            rank.update_points()

    
    def load(self, player1, player2):
        """ Load a game from the database

        Raises
        ------
        ObjectDoesNotExist if a player is not in the database

        Parameters
        ----------
        player1: str
            the username of the first player
        player2: str
            the username of the second player
        """
        players_db = []
        players_db.append(Player.objects.get(username=player1))
        players_db.append(Player.objects.get(username=player2))
        self.players = []
        for p in players_db:
            self.players.append(p.username)
        iplayers = {}
        for p in self.players:
            iplayers[p] = self.players.index(p)

        if LastGame.objects.filter(player1=players_db[0],
                                   player2=players_db[1]).exists():
            all_cells = LastGame.objects.filter(player1=players_db[0],
                                                player2=players_db[1])
        else:
            all_cells = LastGame.objects.filter(player1=players_db[1],
                                                player2=players_db[0])

        for game_cell in all_cells:
            self.grid[game_cell.cell_line, game_cell.cell_column] \
                = iplayers[game_cell.cell_player.username]
            
        

class PlayerDBException(Exception):
    pass

class ImbalancedGameException(Exception):
    pass

class SeveralWinnersException(Exception):
    pass
